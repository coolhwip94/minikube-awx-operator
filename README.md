# Minikube AWX Setup

## Install Kubectl
> Not technically needed since it is included in minikube but will help with compatibility when running AWX deployment.
```
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list

sudo apt-get update

sudo apt-get install -y kubectl
```

## Install dependencies
```
sudo apt-get install conntrack
```

### Install docker
```
sudo apt-get install -y docker.io
```
### Install Minikube
```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64

chmod +x minikube

sudo mv minikube /usr/local/bin
```

## Set subnets that will not go through the proxy.
Defaults used by minikube that should be included
> https://minikube.sigs.k8s.io/docs/handbook/vpn_and_proxy/

* 192.168.59.0/24: Used by the minikube VM. Configurable for some hypervisors via —host-only-cidr
* 192.168.39.0/24: Used by the minikube kvm2 driver.
* 192.168.49.0/24: Used by the minikube docker driver’s first cluster.
* 10.96.0.0/12: Used by service cluster IP’s. Configurable via —service-cluster-ip-range
* Include your LAN Subnet as well to be able to access from your network.

```
set NO_PROXY=localhost,127.0.0.1,10.96.0.0/12,192.168.49.0/24,192.168.59.0/24,192.168.1.0/24
```

Start the cluster
```
minikube start --vm-driver=none --docker-env NO_PROXY=$NO_PROXY
```

## Clone AWX-operator Repo
```
git clone https://github.com/ansible/awx-operator.git
```

## Checkout to release version
> This lab will use 0.14.0, newer versions are failing to deploy the correct pods at the time of this writing.
```
cd awx-operator
git checkout 0.14.0
```

## Set a namespace and Deploy operator
> Using `my-awx` as the namespace for this tutorial. 
> The deployment playbooks will use this namespace
```
export NAMESPACE=my-awx
make deploy
```

Check that `awx-operator` is running
```
kubectl get pods -n my-awx
```

## Set current context
> Set the current context so you don’t have to define the namespace for every command

```
kubectl config set-context --current --namespace==my-awx
```

## Deploy AWX Instance
> Earlier you deployed the awx operator, now we need to deploy the awx instance
```
kubectl apply -f awx-demo.yml
```

> The `awx-demo.yml` should be included in the repo, but if it is not, here is the contents
```yaml
---
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx-demo
spec:
  service_type: nodeport
```

Viewing logs
```
kubectl logs -f deployments/awx-operator-controller-manager -c manager
```

Viewing new resources
```
kubectl get pods -l "app.kubernetes.io/managed-by=awx-operator"
NAME                        READY   STATUS    RESTARTS   AGE
awx-demo-77d96f88d5-pnhr8   4/4     Running   0          3m24s
awx-demo-postgres-0         1/1     Running   0          3m34s

kubectl get svc -l "app.kubernetes.io/managed-by=awx-operator"
NAME                TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
awx-demo-postgres   ClusterIP   None           <none>        5432/TCP       4m4s
awx-demo-service    NodePort    10.109.40.38   <none>        80:31006/TCP   3m56s
```

## Access AWX Instance
AWX will be accessible by running this command
```
minikube service awx-demo-service --url -n my-awx
```
Output will be similar to this (visit this url from within your local network):
```
http://192.168.1.237:30929
```


## Default Credentials
The default user is `admin`

To retrieve the password
```
kubectl get secret awx-demo-admin-password -o jsonpath="{.data.password}" | base64 --decode
```

